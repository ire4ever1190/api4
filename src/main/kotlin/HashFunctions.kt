package me.jakeleahy.api4

import java.security.MessageDigest


fun String.toMD5(): String {
    val bytes = MessageDigest.getInstance("MD5").digest(this.toByteArray())
    return bytes.toHex()
}

fun String.toSHA1(): String {
    val bytes = MessageDigest.getInstance("SHA1").digest(this.toByteArray())
    return bytes.toHex()
}

fun ByteArray.toHex(): String {
    return joinToString("") { "%02x".format(it) }
}