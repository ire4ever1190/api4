package me.jakeleahy.api4.DataClasses

import kotlinx.serialization.Serializable

/**
 * This is the JSON that is sent to App4 when logging in
 * @property userKey Your username
 * @property passwordAD MD5 hash of your password
 * @property passwordHash SHA1 hash of your password
 * @property appVersionNo Version of the app
 * @property iosVersionNo Version of device
 * @property deviceType What kind of device is logging in
 */
//TODO make appversion a constant
@Serializable
data class Login(
    val userKey: String,
    val passwordAD: String,
    val passwordHash: String,
    val PushID: String? = null,
    val appVersionNo: String = "S3.1",
    val iosVersionNo: String = "6.2.9200.0",
    val deviceType: String = "Microsoft Windows NT 6.2.9200.0"
)

@Serializable
data class AltLogin(
    val userID: Int,
    val userToken: String,
    val dateCheck: String = "2000-01-01 00:00:00",
    val appVersionNo: String = "S3.1"
)

@Serializable
data class AltSyncLogin(
    val Token: Int,
    val UserID: Int,
    val LastSyncDate: String = "2000-01-01 00:00:00",
    val appVersionNo: String = "S3.1",
    val PushID: String? = null,
    val iosVersionNo: String = "Microsoft Windows NT 6.2.9200.0"
)
