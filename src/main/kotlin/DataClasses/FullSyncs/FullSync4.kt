package me.jakeleahy.api4.DataClasses.FullSyncs

import kotlinx.serialization.Serializable
import me.jakeleahy.api4.DataClasses.Homework

@Serializable
data class FullSync4(
    val tblHomework: List<Homework>
)