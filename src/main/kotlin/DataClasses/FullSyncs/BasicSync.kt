package me.jakeleahy.api4.DataClasses.FullSyncs

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonParametricSerializer
import kotlinx.serialization.modules.SerializersModule
import me.jakeleahy.api4.DataClasses.AltSyncLogin
import me.jakeleahy.api4.DataClasses.Homework


@Serializable
data class AddHomeworkSyncContent(
    val loginData: LoginBasicSync,
    val homeworkData: HomeworkBasicSync
)

@Serializable
data class LoginBasicSync(
    val TableName: String = "header",
    val Rows: List<AltSyncLogin>
)


@Serializable
data class HomeworkBasicSync (
    val TableName: String = "tblHomework",
    val Rows: List<Homework>
)
