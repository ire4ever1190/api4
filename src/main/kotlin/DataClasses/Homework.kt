package me.jakeleahy.api4.DataClasses

import kotlinx.serialization.Serializable

/**
 * `Homework` holds information relating to a piece of homework \n
 * All dates are in the format YYYY/MM/DD
 * @property Task The name of the homework task
 * @property FullDescription The extra information about the homework task
 * @property IsDeleted Whether the homework has been deleted or not
 * @property Due Date of when the homework is due
 * @property Started Date of when the student started
 * @property Completed Date of when the student completed homework
 * @property ApprovedBy Name of guardian who approved task completion
 * @property ApprovedOn Date of when task was approved
 * @property ClassID The class that the homework was set for
 * @property ACTION I have not found all of these out yet
 * @property ID The ID of the homework task
 */
@Serializable
data class Homework(
    val Task: String,
    val FullDescription: String,
    val Resources: String? = null,
    val IsDeleted: Boolean? = false,
    val Due: String,
    val Started: String?,
    val Completed: String?,
    val ApprovedOn: String?,
    val ApprovedBy: String?,
    val CreatorID: Int,
    val ClassID: Int,
    val Opened: String,
    val ACTION: String,
    val ID: String
)