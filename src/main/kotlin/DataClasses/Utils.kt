package me.jakeleahy.api4.DataClasses

import java.text.SimpleDateFormat
import java.util.*

// Todo find out name of date format
fun Date.toApp4(): String {
    return SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this)
}

// App4 got some weird dates
fun Date.formatDateTimeDigits(): String {
    return SimpleDateFormat("yyyyMMdd.HHmmss").format(this)
}

fun Date.toyyyyMMdd(): String {
    return SimpleDateFormat("yyyy-MM-dd").format(this)
}