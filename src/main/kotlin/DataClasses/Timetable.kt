package me.jakeleahy.api4.DataClasses

import kotlinx.serialization.Serializable

@Serializable
data class FullSync5(
    val tblTimeTable: List<Timetable>
)

@Serializable
data class Timetable(
    val ID: Int,
    val DayNo: Int,
    val PeriodNo: String,
    val StartTime: Int,
    val RoomName: String?,
    val SubjectName: String?,
    val TeacherID: Int?,
    val TeacherName: String,
    val ClassID: Int?
)