package me.jakeleahy.api4

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.coroutines.awaitString
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import me.jakeleahy.api4.DataClasses.*
import me.jakeleahy.api4.DataClasses.FullSyncs.*
import java.lang.Integer.parseInt
import java.util.*

/**
 * An instance of an app4 accounts, used to get information
 *  @property loginDetails Is returned from the login and can be used instead of username/password to remove need to login
 *  @property schoolIdentifier The shortened version of your school name. Found in your app4 url i.e schoolid.app4.ws
 */

class App4(val schoolIdentifier: String, var loginDetails: AltLogin? = null) {
    // Every api request goes to same endpoint
    private val API_END_POINT = "https://$schoolIdentifier.app4.ws/processStudent47.php"
    private val json = Json(JsonConfiguration.Stable.copy(ignoreUnknownKeys = true))

    /**
     * Gets the login token along with information about the school
     * @param username Your username for app4
     * @param password Your password for app4
     */
    suspend fun login(username: String, password: String): AltLogin {
        val passwordMD5 = password.toMD5()
        val passwordSHA1 = password.toSHA1()
        val loginData = Login(username, passwordMD5, passwordSHA1)
        val response = Fuel.post(
            API_END_POINT,
            listOf(
                "action" to Actions.LOGINCHECK,
                "content" to Json.stringify(Login.serializer(), loginData)
            )
        ).awaitString()
        // There is more info but the json is kind of malformed for this purpose
        val responseJson = Json.parseJson(response)
        val infoObject = responseJson.jsonArray[0].jsonObject["Rows"]!!.jsonArray[0].jsonObject
        this.loginDetails = AltLogin(parseInt(infoObject["USERID"].toString()), infoObject["TOKEN"].toString())
        return this.loginDetails!!
    }


    /**
     * This is the base function that sends the request to app4
     * @param action The action that you want to do with the api
     * @param content The body to send for said action
     * @return Returns the string response from app4
     */
    private suspend fun apiCall(action: String, content: String): String {
        val payload = listOf(
            "action" to action,
            "content" to content
        )
        return Fuel.post(API_END_POINT, payload).awaitString()
    }

    /**
     * Get's a specified fullsync from app4
     * @param number relates to what you want to get
     *
     */
    @OptIn(UnstableDefault::class)
    private suspend fun fullSync(number: Int): String {
        val content =  Json.stringify(AltLogin.serializer(), this.loginDetails!!)
        val response = apiCall("FULLSYNC$number", content)
        return fixJsonTables(response)
    }

    /**
     * Gets your timetable from app4
     * @return Returns the JSON for timetable
     */
    suspend fun getTimetable(): FullSync5 {
        val response = fullSync(5)
        return json.parse(FullSync5.serializer(), response)
    }

    /**
     * Gets all the homework that is on app4
     * @return Returns the JSON for all the homework
     */
    suspend fun getHomework(): FullSync4 {
        val response = fullSync(4)
        return json.parse(FullSync4.serializer(), response)
    }

    /**
     * Adds a specified homework task to App4, Can also be used to remove a homeowrk task from app4
     * @param task The homework task that you want to add
     * @return Returns a json string containing the task that you sent, this can be ignored
     */
    @ImplicitReflectionSerializer
    suspend fun addHomework(task: Homework): String {
        val lastSyncDate = Date.from(Date().toInstant().plusSeconds(500))
        val payload = AddHomeworkSyncContent(
            LoginBasicSync(
                Rows = listOf(AltSyncLogin(
                    parseInt(this.loginDetails!!.userToken),
                    this.loginDetails!!.userID,
                    LastSyncDate = lastSyncDate.toApp4())
                )
            ),
            HomeworkBasicSync(
                Rows = listOf(task)
            )
        )
        val jsonData = json.stringify(AddHomeworkSyncContent.serializer(), payload)
        val fixedPayload = createPolyList(jsonData)
        return apiCall(Actions.WRITEAPPAUD, fixedPayload)
    }

    /**
     * Makes App4 compatible ids
     * @return App4 compatible idd
     */
    fun makeID(): String {
        val firstPart = (0..5).map {
            this.loginDetails!!.userID.toString().getOrNull(it) ?: 0
        }.reversed().joinToString("")
        // Third and second part are not exact replicas of original code
        val secondPart = Date().formatDateTimeDigits()
        val thirdPart = (0..200).random()
        val final = "$firstPart-$secondPart.$thirdPart"

        return final
    }


    /**
     * Makes a normal json object be turned into a list of different objects. Mainly used for syncing information with
     * App4
     * @param json The json that is to be transformed
     * @return The transformed json string
     */
    private fun createPolyList(json: String): String {
        val jsonKeysRegex = Regex("(\"\\w+\":)\\{\"TableName\"")
        var newJson = json
        val parsedKeys = jsonKeysRegex.findAll(json)
        parsedKeys.forEach {
            val key = it.groupValues[1]
            newJson = newJson.replace(key, "")
        }
        newJson = "[" + newJson.substring(1, newJson.length - 1) + "]"
        return newJson
    }

    /**
     * Fixes the json that app4 returns so it is easily serializable
     * @param json The Json string that you want to fix up
     * @return The transformed json string
     */
    @OptIn(UnstableDefault::class)
    private fun fixJsonTables(json: String): String {
        // Finds the table names
        val tableRowRegex = Regex("\"TableName\":\"(\\w+)\",")
        val jsonParsed = Json.parseJson(json)
        var fixedJson = ""
        jsonParsed.jsonArray.forEach {
            val tableName = tableRowRegex.find(it.toString())!!.groupValues[1]
            val rowReplaceString = it.toString().replace("Rows", tableName)
            val tableKeyRemoved = tableRowRegex.replace(rowReplaceString, "")
            fixedJson += tableKeyRemoved.removePrefix("{").removeSuffix("}") + ","
        }
        fixedJson = fixedJson.removeSuffix(",")
        return "{$fixedJson}"
    }
}