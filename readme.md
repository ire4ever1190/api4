[![Release](https://jitpack.io/v/User/Repo.svg)](https://jitpack.io/ire4ever1190/Repo)
This is a kotlin library for app4
#### It requires an app4 account

to use you do 
```
val app4 = App4("schoolidentifier")
val token = app4.login("johnsmith", "hunter2")
// You can save the token that it returns to save on times you need to login
println(app4.getTimetable)
```
